import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Post} from "../models/post.model";
import {SearchOptions} from "../models/search-options.model";

@Injectable({
    providedIn: 'root'
})
export class PostHttpService {

    baseUrl: string;

    constructor(private httpClient: HttpClient) {
        this.baseUrl = environment.backend_url + '/posts';
    }

    search(searchOptions: SearchOptions): Observable<Post[]> {
        return this.httpClient.get<Post[]>(
            this.baseUrl + "/search",
            {
                params: {...searchOptions}
            }
        );
    }
}
