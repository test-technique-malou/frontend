import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private datesSubject = new BehaviorSubject<Date[]>([]);
    dates$ = this.datesSubject.asObservable();

    constructor() { }

    updateDates(dates: Date[]) {
        this.datesSubject.next(dates);
    }

    getCurrentDates() {
        return this.datesSubject.getValue();
    }

}
