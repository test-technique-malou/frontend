export interface SearchOptions {
    postedAfter?: string;
    postedBefore?: string;
}
