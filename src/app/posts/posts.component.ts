import { Component, OnInit } from '@angular/core';
import {PostHttpService} from "../../services/post-http.service";
import {map, Observable, switchAll, switchMap} from "rxjs";
import {Post} from "../../models/post.model";
import {DataService} from "../../services/data.service";
import {returnObjectIfValueNotNil} from "../utils/util";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts: Post[] | undefined;

  constructor(
      private postService: PostHttpService,
      private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.dataService.dates$.pipe(
        switchMap(dates => {
          return this.postService.search({
            ...(returnObjectIfValueNotNil('startDate', dates[0]?.toISOString())),
            ...(returnObjectIfValueNotNil('endDate', dates[1]?.toISOString()))
          })
        })
    ).subscribe(next => {
      this.posts = next;
    })
  }

}
