import { Component, OnInit } from '@angular/core';
import {Observable, of, Subject} from "rxjs";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  datePickerModel: Date[] = [];

  constructor(
      private dataService: DataService
  ) { }

  ngOnInit(): void {
  }

  resetDatePicker() {
    this.datePickerModel = [];
  }

  onDatePickerValueChange(event: any) {
    this.dataService.updateDates(event);
  }

}
