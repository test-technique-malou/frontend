export function returnObjectIfValueNotNil(key: string, value: any): any {
    return value != null ? {[key]: value} : {};
}
